Feature: Realizar um novo POST no Facebook

  Scenario Outline: Adicionar um novo POST no Facebook
    Given Dado o "<Browser>" e navegue no site www.facebook.com.br
    And Fazer o "<Login>" e "<Password>" e valide o "<Name>"
    And Selecione o nome  "<Name>" no facebook
    And Validar o Timeline "<Name>"
    When Escrever um novo POST com conteudo "<Input>"
    Then Adicione um smile do grupo "<Grupo>" e icone "<Icone>"
    Then Adicione a localizacao "<Location>"
    Then Enviar o novo POST e validar o conteudo "<Input>"
    Then Valide a localizacao da Postagem "<Location>"
    Then Clique em Like "<LikeText>" no seu POST e valida o "<LikeResult>"
    Then Deletar a nova postagem no facebook e validar a exclusao "<Input>".
    Then Fazer o logout no facebook e validar o texto "<Logout>".

    Examples: 
      | Browser | Input                                       | Login                         | Password | Name              | LikeText | LikeResult        | Grupo       | Icone       | Logout               | Location        |
      | Firefox | Selenium WebDriver Test1234567890           | reinaldo.rossetti@outlook.com |          | Reinaldo Junior   | Curtir   | Você curtiu isso. | sentindo-se | muito feliz | entre ou cadastre-se | Manaus Amazonas |
      | Firefox | ^ symbols Selenium !$%^&test()-_+=~`´+=[]{} | reiload@yahoo.com.br          |          | Reinaldo Rossetti | Like     | You like this.    | feeling     | happy       | Log In or Sign Up    | Berlin, Germany |
