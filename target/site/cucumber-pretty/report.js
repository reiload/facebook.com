$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("my_features.feature");
formatter.feature({
  "line": 1,
  "name": "Realizar um novo POST no Facebook",
  "description": "",
  "id": "realizar-um-novo-post-no-facebook",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 3,
  "name": "Adicionar um novo POST no Facebook",
  "description": "",
  "id": "realizar-um-novo-post-no-facebook;adicionar-um-novo-post-no-facebook",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "Dado o \"\u003cBrowser\u003e\" e navegue no site www.facebook.com.br",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "Fazer o \"\u003cLogin\u003e\" e \"\u003cPassword\u003e\" e valide o \"\u003cName\u003e\"",
  "keyword": "And "
});
formatter.step({
  "line": 6,
  "name": "Selecione o nome  \"\u003cName\u003e\" no facebook",
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "Validar o Timeline \"\u003cName\u003e\"",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "Adicionar o novo POST com conteudo \"\u003cInput\u003e\"",
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "Deletar a nova postagem no facebook",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "Fazer o logout no facebook e validar.",
  "keyword": "Then "
});
formatter.examples({
  "line": 12,
  "name": "",
  "description": "",
  "id": "realizar-um-novo-post-no-facebook;adicionar-um-novo-post-no-facebook;",
  "rows": [
    {
      "cells": [
        "Browser",
        "Input",
        "Login",
        "Password",
        "Name"
      ],
      "line": 13,
      "id": "realizar-um-novo-post-no-facebook;adicionar-um-novo-post-no-facebook;;1"
    },
    {
      "cells": [
        "Firefox",
        "Selenium WebDriver Test1234567890",
        "reinaldo.rossetti@outlook.com",
        "",
        "Reinaldo Junior"
      ],
      "line": 14,
      "id": "realizar-um-novo-post-no-facebook;adicionar-um-novo-post-no-facebook;;2"
    },
    {
      "cells": [
        "Firefox",
        "^ symbols !$%^\u0026test()-_+\u003d~`´+\u003d[]{}",
        "reiload@yahoo.com.br",
        "",
        "Reinaldo Rossetti"
      ],
      "line": 15,
      "id": "realizar-um-novo-post-no-facebook;adicionar-um-novo-post-no-facebook;;3"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 14,
  "name": "Adicionar um novo POST no Facebook",
  "description": "",
  "id": "realizar-um-novo-post-no-facebook;adicionar-um-novo-post-no-facebook;;2",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "Dado o \"Firefox\" e navegue no site www.facebook.com.br",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "Fazer o \"reinaldo.rossetti@outlook.com\" e \"\" e valide o \"Reinaldo Junior\"",
  "matchedColumns": [
    2,
    3,
    4
  ],
  "keyword": "And "
});
formatter.step({
  "line": 6,
  "name": "Selecione o nome  \"Reinaldo Junior\" no facebook",
  "matchedColumns": [
    4
  ],
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "Validar o Timeline \"Reinaldo Junior\"",
  "matchedColumns": [
    4
  ],
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "Adicionar o novo POST com conteudo \"Selenium WebDriver Test1234567890\"",
  "matchedColumns": [
    1
  ],
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "Deletar a nova postagem no facebook",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "Fazer o logout no facebook e validar.",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "Firefox",
      "offset": 8
    }
  ],
  "location": "StepsDefinition.dado_o_e_navegue_no_site_www_facebook_com_br(String)"
});
formatter.result({
  "duration": 19060596825,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "reinaldo.rossetti@outlook.com",
      "offset": 9
    },
    {
      "val": "",
      "offset": 43
    },
    {
      "val": "Reinaldo Junior",
      "offset": 57
    }
  ],
  "location": "StepsDefinition.fazer_o_e_e_valide_o(String,String,String)"
});
formatter.result({
  "duration": 32913418556,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Reinaldo Junior",
      "offset": 19
    }
  ],
  "location": "StepsDefinition.selecione_o_nome_no_facebook(String)"
});
formatter.result({
  "duration": 14475616610,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Reinaldo Junior",
      "offset": 20
    }
  ],
  "location": "StepsDefinition.validar_o_Timeline(String)"
});
formatter.result({
  "duration": 3082621910,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Selenium WebDriver Test1234567890",
      "offset": 36
    }
  ],
  "location": "StepsDefinition.adicionar_o_novo_POST_com_conteudo(String)"
});
formatter.result({
  "duration": 14633239550,
  "status": "passed"
});
formatter.match({
  "location": "StepsDefinition.deletar_a_nova_postagem_no_facebook()"
});
formatter.result({
  "duration": 12801153846,
  "status": "passed"
});
formatter.match({
  "location": "StepsDefinition.fazer_o_logout_no_facebook_e_validar()"
});
formatter.result({
  "duration": 12085322467,
  "status": "passed"
});
formatter.after({
  "duration": 4528139471,
  "status": "passed"
});
formatter.scenario({
  "line": 15,
  "name": "Adicionar um novo POST no Facebook",
  "description": "",
  "id": "realizar-um-novo-post-no-facebook;adicionar-um-novo-post-no-facebook;;3",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "Dado o \"Firefox\" e navegue no site www.facebook.com.br",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "Fazer o \"reiload@yahoo.com.br\" e \"\" e valide o \"Reinaldo Rossetti\"",
  "matchedColumns": [
    2,
    3,
    4
  ],
  "keyword": "And "
});
formatter.step({
  "line": 6,
  "name": "Selecione o nome  \"Reinaldo Rossetti\" no facebook",
  "matchedColumns": [
    4
  ],
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "Validar o Timeline \"Reinaldo Rossetti\"",
  "matchedColumns": [
    4
  ],
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "Adicionar o novo POST com conteudo \"^ symbols !$%^\u0026test()-_+\u003d~`´+\u003d[]{}\"",
  "matchedColumns": [
    1
  ],
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "Deletar a nova postagem no facebook",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "Fazer o logout no facebook e validar.",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "Firefox",
      "offset": 8
    }
  ],
  "location": "StepsDefinition.dado_o_e_navegue_no_site_www_facebook_com_br(String)"
});
formatter.result({
  "duration": 14064258173,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "reiload@yahoo.com.br",
      "offset": 9
    },
    {
      "val": "",
      "offset": 34
    },
    {
      "val": "Reinaldo Rossetti",
      "offset": 48
    }
  ],
  "location": "StepsDefinition.fazer_o_e_e_valide_o(String,String,String)"
});
formatter.result({
  "duration": 63741608059,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Reinaldo Rossetti",
      "offset": 19
    }
  ],
  "location": "StepsDefinition.selecione_o_nome_no_facebook(String)"
});
formatter.result({
  "duration": 19098213051,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Reinaldo Rossetti",
      "offset": 20
    }
  ],
  "location": "StepsDefinition.validar_o_Timeline(String)"
});
formatter.result({
  "duration": 3096825565,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "^ symbols !$%^\u0026test()-_+\u003d~`´+\u003d[]{}",
      "offset": 36
    }
  ],
  "location": "StepsDefinition.adicionar_o_novo_POST_com_conteudo(String)"
});
formatter.result({
  "duration": 15265064698,
  "status": "passed"
});
formatter.match({
  "location": "StepsDefinition.deletar_a_nova_postagem_no_facebook()"
});
formatter.result({
  "duration": 12802439474,
  "status": "passed"
});
formatter.match({
  "location": "StepsDefinition.fazer_o_logout_no_facebook_e_validar()"
});
formatter.result({
  "duration": 14038446923,
  "status": "passed"
});
formatter.after({
  "duration": 4131958936,
  "status": "passed"
});
});