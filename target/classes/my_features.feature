Feature: Realizar um novo POST no Facebook

  Scenario Outline: Adicionar um novo POST no Facebook
    Given Dado o "<Browser>" e navegue no site www.facebook.com.br
    And Fazer o "<Login>" e "<Password>" e valide o "<Name>"
    And Selecione o nome  "<Name>" no facebook
    And Validar o Timeline "<Name>"
    When Adicionar o novo POST com conteudo "<Input>"
    Then Deletar a nova postagem no facebook e validar a exclusao "<Input>".
    Then Fazer o logout no facebook e validar.

    Examples: 
      | Browser | Input                              | Login                         | Password | Name              |
      | Firefox | Selenium WebDriver Test1234567890  | reinaldo.rossetti@outlook.com |          | Reinaldo Junior   |
      | Firefox | ^ symbols !$%^&test()-_+=~`´+=[]{} | reiload@yahoo.com.br          |          | Reinaldo Rossetti |
