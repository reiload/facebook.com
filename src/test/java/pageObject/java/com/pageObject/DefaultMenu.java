package pageObject.java.com.pageObject;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import pageObject.java.util.functions;

public class DefaultMenu extends AbstractPage {

	public DefaultMenu(WebDriver driver) {
		super(driver);
	}

	public final HomePage loginContainer = PageFactory.initElements(driver, HomePage.class);

	// delay set on properties file.
	public functions functions = new functions();
	public String delay_w = functions.getConfig("delay_wait");
	public int delay_wait = Integer.parseInt(delay_w);
	
	public  DefaultMenu selectText(String user_name) throws InterruptedException {
	
		
		//Thread.sleep(delay_sleep);
		//driver.switchTo().frame("iframeLogin");
		System.out.println("select Text user name");
		// click element by @FindBy use CSS
		// if fail do with XPath
		
		try{ 
			loginContainer.user_text_css.click();
			//Thread.sleep(delay_sleep);
			loginContainer.user_name_css.getText();
		}
		catch(Exception e){
			System.out.println("Error to select Text user name");
			functions.searchAndClick(By.xpath("//div[@id='userNav']/ul/li[1]/a/div/span"), delay_wait);
		}	
	
		return new DefaultMenu(driver);
		
	}
	
	
	public DefaultMenu validaText(String user_name) throws InterruptedException {
	
		String get_text;
		System.out.println("validate Text on Default Menu");
		
		// get the text in main menu on facebook.
	
		try{ 
			get_text = loginContainer.user_name_css.getText();
		}
		catch(Exception e){
			functions.alertConfirm();
			get_text = functions.getText(By.xpath(".//*[@id='fb-timeline-cover-name']"), delay_wait);
		}	
		
		System.out.println("user name: " + get_text);
		Assert.assertEquals(user_name, get_text);
		return new DefaultMenu(driver);
	}
	
}
