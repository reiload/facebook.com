package pageObject.java.com.pageObject;

import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;


import pageObject.java.util.functions;

public class timelinePage extends AbstractPage{

	
	public timelinePage(WebDriver driver) {
		super(driver);
	}
	
	public HomePage loginContainer = PageFactory.initElements(driver, HomePage.class);

	// delay set on properties file.
	public functions functions = new functions();
	public String delay_w = functions.getConfig("delay_wait");
	public int delay_wait = Integer.parseInt(delay_w);
	public String delay_i = functions.getConfig("implicitlyWait");
	public int implicitlyWait = Integer.parseInt(delay_i);
	public String textPost;

	public timelinePage escreverText(String text) throws InterruptedException {
		
		System.out.println(text);
		textPost = text;
		//click element by @FindBy
		// if fail do with XPath
		
		try{ 
			System.out.println("click field POST");
			loginContainer.fieldText.sendKeys(text);

		}
		catch(Exception e){
			System.out.println("sendkeys try again");
			driver.navigate().refresh();
			driver.manage().timeouts().implicitlyWait(implicitlyWait, TimeUnit.SECONDS);
			HomePage loginContainer = PageFactory.initElements(driver, HomePage.class);
			loginContainer.fieldText.sendKeys(text);
		}	
		return new timelinePage(driver);
		
	}
	
	
	public timelinePage clickButtonPost() throws InterruptedException {
				
		try{ 
			System.out.println("Click Post Button");
			HomePage loginContainer = PageFactory.initElements(driver, HomePage.class);
			loginContainer.buttonPost.click();
			
			Boolean test_button = functions.searchAndClick(By.cssSelector("._1mf7._4jy0._4jy3._4jy1._51sy.selected._42ft"), delay_wait); 
			System.out.println("found click ButtonPost: "+test_button);
		}
		catch(Exception e){
			
			System.out.println("error in function clickButtonPost");
			
			}	
		return new timelinePage(driver);
		
	}

	public timelinePage validatePost(String text) throws InterruptedException {
		
		int test= 0; 
		try{ 
			
			System.out.println("validate POST");
			text = functions.getTextPageProduct(By.xpath(".//*[contains(@class, 'userContent')]/*[contains(text(),'"+text+"')]"), delay_wait);
			System.out.println("list posts");
			test=functions.validatePost(By.xpath("//*[contains(@class, 'userContent')]/p"), text);
	
		}
		catch(Exception e){
			
			driver.navigate().refresh();
			functions.alertConfirm();
			System.out.println("Error to validate POST, try again");
			test=functions.validatePost(By.xpath("//*[contains(@class, 'userContent')]/p"), text);

		}	
		Assert.assertTrue(test>0);
		return new timelinePage(driver);
		
	}
	public timelinePage LikePOST(String likeText) throws InterruptedException {
			
		try{ 
			
			System.out.println("movetoelement Like");
			functions.searchAndClick(By.xpath("(.//*[contains(@class,'UFILikeLink') and contains(text(),'"+likeText+"')])[1]"),delay_wait);

		}
		catch(Exception e){
			System.out.println("Error in Like");
			functions.ClickByEnter(By.xpath("(.//a[contains(@class,'UFILikeLink')])[1]"), delay_wait);

		}
		return new timelinePage(driver);
		
	}
	
	public timelinePage valideLikePOST(String likeText) throws InterruptedException {
		String text;
		
		try{ 
			System.out.println("valide LikePOST");
			text = functions.getText(By.cssSelector("div.UFILikeSentenceText span span:first-child"), delay_wait);

		}
		catch(Exception e){
			functions.movetoelement(By.xpath(".//*[contains(@class, 'userContent')]/p[contains(text(), '"+textPost+"')]"));
			LikePOST(likeText);
			System.out.println("click Like POST again");
			text = functions.getText(By.cssSelector("div.UFILikeSentenceText span span:first-child"), delay_wait);
		
		}
		
		System.out.println(likeText+" / "+text);
		Assert.assertEquals(likeText,text);
		return new timelinePage(driver);
		
	}
	
	public timelinePage deletePost() throws InterruptedException {
		
		Boolean test= false; 
		try{ 

			System.out.println("delete POST");
			test=functions.deletePost();
	
		}
		catch(Exception e){
			
			driver.navigate().refresh();
			System.out.println("Error to Delete POST, try again");
			test=functions.deletePost();

		}	
		Assert.assertTrue(test);
		return new timelinePage(driver);
		
	}
	
	public timelinePage addSmile(String textSmile,String textSmileFeel) throws InterruptedException {
		

		try{ 
			System.out.println("add Smile");
			
			functions.addSmile(textSmile,textSmileFeel);
	
		}
		catch(Exception e){
			System.out.println("Error to addSmile, try again");

		}
		return new timelinePage(driver);
		
	}

	public timelinePage addLocation(String textLocation) throws InterruptedException {
		

		try{ 
			System.out.println("add Location");
			functions.addLocation(textLocation);
	
		}
		catch(Exception e){
			System.out.println("Error to Location, try again");

		}
		return new timelinePage(driver);
		
	}
	
	public timelinePage validateLocation(String text) throws InterruptedException {
		
		int test= 0; 
		
		try{ 		
			System.out.println("validate Location");
			text = functions.getTextPageProduct(By.xpath(".//*[contains(@class,'profileLink') and contains(text(),'"+text+"')]"), delay_wait);
			test=functions.validatePost(By.xpath(".//*[contains(@class,'profileLink') and contains(text(),'"+text+"')]"), text);
	
		}
		catch(Exception e){
			
			driver.navigate().refresh();
			functions.alertConfirm();
			System.out.println("Error to validate Location, try again");
			test=functions.validatePost(By.xpath(".//*[contains(@class,'profileLink') and contains(text(),'"+ text+"')]"), text);

		}	
		Assert.assertTrue(test>0);
		return new timelinePage(driver);
		
	}
	

	public timelinePage validateDelete(String text) throws InterruptedException {
		
		int test= 0; 
		try{ 
			System.out.println("validating Delete");
			driver.manage().timeouts().implicitlyWait(implicitlyWait, TimeUnit.SECONDS);
			test=functions.validateDelete(By.xpath("//*[contains(@class, 'userContent')]/p"), text);
		}
		catch(Exception e){

			System.out.println("Error to Delete POST, try again");
			driver.navigate().refresh();
			functions.alertConfirm();
			driver.manage().timeouts().implicitlyWait(implicitlyWait, TimeUnit.SECONDS);
			test=functions.validateDelete(By.xpath("//*[contains(@class, 'userContent')]/p"), text);

		}	
		Assert.assertTrue(test==0);
		return new timelinePage(driver);
		
	}
	
}
