package pageObject.java.com.pageObject;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.UnreachableBrowserException;
import pageObject.java.util.functions;


public class AbstractPage {
	
	protected static WebDriver driver;
	
	public AbstractPage(WebDriver driver) {
		AbstractPage.driver = driver;
	}

	public LandingPage navigateToWebSite() throws IOException{
		
		functions config = new functions();
		int sleep = Integer.parseInt(config.getConfig("pageLoadTimeout"));
		driver.manage().window().maximize(); 
		driver.manage().timeouts().pageLoadTimeout(sleep, TimeUnit.SECONDS);
		driver.navigate().to(config.getConfig("website"));
		driver.manage().deleteAllCookies();
		return new LandingPage(driver);
	}
	
	public void closeDriver(){
		
        try {
        	// function to close the browser
            driver.close();
            driver.quit();
               
        } catch (UnreachableBrowserException e) {
        	System.out.println("cannot close browser: unreachable browser: "+ e);
        }
		
	}
		
}
