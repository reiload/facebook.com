package pageObject.java.util;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pageObject.java.com.pageObject.AbstractPage;
import pageObject.java.com.pageObject.LoginPage.StringUtils;
import org.openqa.selenium.support.ui.*;


public class functions extends AbstractPage{

	public functions() {
		super(driver);
	}

	// delay set on properties file.

	public String delay_w = getConfig("delay_wait");
	public int delay_wait = Integer.parseInt(delay_w);
	public String delay_i = getConfig("implicitlyWait");
	public int implicitlyWait = Integer.parseInt(delay_i);
	
	//public object wait= null;
	
    Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
            .withTimeout(delay_wait, TimeUnit.SECONDS)
            .pollingEvery(5, TimeUnit.SECONDS)
            .ignoring(NoSuchElementException.class);
	
	public boolean searchAndClick(By locator, int seconds) {
		
		boolean test = false;
		WebElement search = null;

		try{	
			search = wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(locator)));
			
			//click one element javascript
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", search);
			test = true;

		}catch(Exception e){
			System.out.println("Element not found: " + e);
		}
		return test;
	}
	public boolean search(By locator, int seconds) {
		
		boolean test = false;
		boolean search = false;
		driver.manage().timeouts().implicitlyWait(implicitlyWait, TimeUnit.SECONDS);

		try{	
			search = wait.until(ExpectedConditions.invisibilityOfElementLocated((By) driver.findElement(locator)));
			
			//click one element javascript
			//JavascriptExecutor executor = (JavascriptExecutor)driver;
			//executor.executeScript("arguments[0].select();", search);
			test = true;

		}catch(Exception e){
			System.out.println("Element not found: " + e);
		}
		return test;
	}
	public void searchSmile(By locator, int seconds) {
		
    
		WebElement search = null;

		try{	
			search = wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(locator)));
			//driver.manage().timeouts().implicitlyWait(implicitlyWait, TimeUnit.SECONDS);
			if (search.isEnabled()){

				//click one element javascript
				JavascriptExecutor executor = (JavascriptExecutor)driver;
				executor.executeScript("arguments[0].click();", search);
				driver.manage().timeouts().implicitlyWait(implicitlyWait, TimeUnit.SECONDS);
			};
		}catch(Exception e){
			System.out.println("Element not found");
		}
	}
	
	public boolean ClickByEnter(By locator, int seconds) {
		
		boolean Product = false;
		WebElement search = null;
		// To do change elementToBeClickable.
		try{
			search = wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(locator)));
			search.sendKeys(Keys.ENTER);
			
		}catch (Exception e){
			System.out.println("Element not found!");
		}
		return Product;
	}
	
	public String getText(By locator, int seconds) {
		
		String product_text = "Not Found";
		WebElement search = null;
		try{
			
			search = wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(locator)));
			if (search.isEnabled()){
				product_text = search.getText();
		
			};
			
		}catch(Exception e){
			System.out.println("\nElement not found! " + e);
		}
		return product_text;
	}
	
	
	public boolean click(By by, Integer seconds) throws InterruptedException{
			
		try{
			
			wait.until(ExpectedConditions.elementToBeClickable((WebElement) by)).click();
			return true;
			
		}catch (Exception e){
			
			
			System.out.println("Element not found!"); 
			return false;
		
		}
	}
	
	public boolean sendKeys(By by, int seconds, String text) throws InterruptedException{
		Boolean test = false;
		
		try{
			
			WebElement myDynamicElement = wait.until(ExpectedConditions.presenceOfElementLocated(by));
			myDynamicElement.findElement(by).sendKeys(text);
			test = true;
			
		}catch (Exception e){
			
			System.out.println("Element not found!"); 
		
		}
		return test;
	}
	
	public void sendLocation(By by, int seconds, String text) throws InterruptedException{

		//to do add wait dynamic
		
		try{
			WebElement textbox = driver.findElement(by);
			Actions act = new Actions(driver);
			act.keyUp(Keys.SHIFT).sendKeys(textbox, text).keyDown(Keys.SHIFT)
			                .build().perform();

			System.out.println("Select field text.");
			//to do select by name.
			//listItems.get(1).click();
			searchAndClick(By.className("xhpc_message_text"), delay_wait);

			
		}catch (Exception e){
			
			System.out.println("Element not found in sendkeys!\n" + e + "\n" + e.getLocalizedMessage()); 
		}

	}	

	public static String getTextPageProduct(By locator, Integer seconds) throws InterruptedException{
		String text = "Text element not found!";
		WebDriverWait wait = new WebDriverWait(driver, seconds);
		System.out.println(locator);
		
		try{
			
			text = wait.until(ExpectedConditions.presenceOfElementLocated(locator)).getText();
			System.out.println("get text with success!");
			return text;
			
		}catch (Exception e){
			
			System.out.println("Text element not found!"); 
			return text;
		
		}
	}
	
	public String refleshPage(By locator, Integer seconds) throws InterruptedException{
		
		String text = "Element not found";
		
		try{
			driver.navigate().refresh();
			driver.switchTo().alert().accept();
			text = wait.until(ExpectedConditions.presenceOfElementLocated(locator)).getText();
			
		}catch (Exception e){
			
			System.out.println("Element not found, try again!"); 
			driver.navigate().refresh();
			alertConfirm();
			text = wait.until(ExpectedConditions.presenceOfElementLocated(locator)).getText();
		
		}
		return text;
	}
	
	public String alertConfirm() {


		String alertText = "";
		try {
			Alert alert = driver.switchTo().alert();
			alertText = alert.getText();
			alert.dismiss();
			// or accept as below
			//alert.accept();

		} catch (NoAlertPresentException nape) {
			// nothing to do, because we only want to close it when pop up
			System.out.println("Alert not found!");

		}
		return alertText;
		
	}
	public Boolean  deletePost() throws InterruptedException {

		Boolean test= false;
		//To do make with css selector
		
		try {
			System.out.println("Delete POST");
			driver.manage().timeouts().implicitlyWait(implicitlyWait, TimeUnit.SECONDS);
			searchAndClick(By.xpath(".//*[@id='recent_capsule_container']/div/div/div/div[1]/div/div/div/div/div/a"),delay_wait);
			driver.manage().timeouts().implicitlyWait(implicitlyWait, TimeUnit.SECONDS);
			searchAndClick(By.xpath("//*[contains(@data-feed-option-name, 'FeedDeleteOption')]/span/span"),delay_wait);
			driver.manage().timeouts().implicitlyWait(implicitlyWait, TimeUnit.SECONDS);
			searchAndClick(By.cssSelector(".layerConfirm.uiOverlayButton._4jy3._4jy1.selected"),delay_wait);
			test= true;

		} catch (NoAlertPresentException nape) {
			// nothing to do, because we only want to close it when pop up
			System.out.println("Fail to Delete POST");

		}
		return test;
		
	}
	
	public void  addSmile(String textSmile,String textSmileFeel) throws InterruptedException {
	
		try {
			System.out.println("add Smile");
			driver.manage().timeouts().implicitlyWait(implicitlyWait, TimeUnit.SECONDS);
			searchSmile(By.xpath(".//*[@class='clearfix']/div[contains(@class,'lfloat')]/div/a[2]/div"),delay_wait);
			System.out.println("Send Keys textSmile");
			driver.manage().timeouts().implicitlyWait(implicitlyWait, TimeUnit.SECONDS);
			sendKeys(By.xpath(".//*[contains(@id,'js')]/input[(@type='text')]"),delay_wait,textSmile);
			driver.manage().timeouts().implicitlyWait(implicitlyWait, TimeUnit.SECONDS);
			ClickByEnter(By.xpath(".//*[contains(@id,'js')]/input[(@type='text')]"),delay_wait);
			
			System.out.println("Send Keys textSmileFeel");
			sendKeys(By.xpath(".//*[contains(@id,'js')]/input[(@type='text')]"),delay_wait,textSmileFeel);
			ClickByEnter(By.xpath(".//*[contains(@id,'js')]/input[(@type='text')]"),delay_wait);
			
		} catch (NoAlertPresentException e) {
			// nothing to do, because we only want to close it when pop up
			System.out.println("Fail to create Smile: "+e);

		}
		
	}

	public void  addLocation(String textLocation) throws InterruptedException {
		
		try {
			//To do change xpath to css.
			System.out.println(textLocation);
			driver.manage().timeouts().implicitlyWait(implicitlyWait, TimeUnit.SECONDS);
			searchSmile(By.xpath(".//*[@class='clearfix']/div[contains(@class,'lfloat')]/div/a[3]/div"),delay_wait);
			System.out.println("Send Keys Location");
			ClickByEnter(By.xpath(".//*[contains(@id,'js')]/input[(@type='text')]"),delay_wait);
			System.out.println("sendKeysWebElement");
			sendLocation(By.xpath(".//*[contains(@id,'js')]/input[(@type='text')]"),delay_wait,textLocation);
			//searchAndClick(By.xpath("(.//button[@type='submit' and @data-testid='react-composer-post-button'])[1]"), delay_wait); 
			
		} catch (NoAlertPresentException e) {

			System.out.println("Fail to create Smile "+e);

		}
		
	}	
	
	public String getConfig(String name)  {
		
		InputStream inputStream = null;
		String value = null;
		try {
			Properties prop = new Properties();
			String propFileName = "conf.properties";
 
			inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
 
			if (inputStream != null) {
				prop.load(inputStream);
			} else {
				throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
			}
 
			// get the property value and print it out
			value = prop.getProperty(name);
 			//System.out.println(value);
			return value;

		} catch (Exception e) {
			
			System.out.println("Exception: " + e);
		
		} 
		
		return value;
		
	}
	
	public void setPropertiesKey(String publicKey ) throws IOException, URISyntaxException {
		
		InputStream inputStream = null;
		Properties prop = new Properties();
		String propFileName = "conf.properties";
 
			inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
			
			if (inputStream != null) {
				prop.load(inputStream);
				inputStream.close();
			} else {
				throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
			}

			prop.setProperty("publicKey", publicKey);
		URL url = getClass().getClassLoader().getResource(propFileName);
		prop.store(new FileOutputStream(new File(url.toURI())), null);

	}



	public int validatePost(By locator,String text) {

		int count_p = 0;
		int count = 0;

		try{

			System.out.println(locator);		
			List<WebElement> AllSearchResults=driver.findElements(locator);
	
			
			for(WebElement eachResult:AllSearchResults)
			   {
				System.out.println("eachResult.getText:" + eachResult.getText());  
				
				  if (eachResult.getText().equalsIgnoreCase(text))
					{
					    count++;
					    System.out.println("Search found: " + eachResult.getText());
						System.out.println("Produto id: " + count);
						count_p  = count;
						
					}
				  //System.out.println("str01" + eachResult.getText()+ " str02:"+text);
			   }
		}catch(Exception e){
			System.out.println("erro in validate post" +e);
		}
		return count_p;
	}
	
	
	public int validateDelete(By locator,String text) throws InterruptedException {

		int count_p = 0;
		int count = 0;
		//driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		Thread.sleep(7000);
	
		try{

			System.out.println(locator);
			
			List<WebElement> AllSearchResults=driver.findElements(locator);
	
			
			for(WebElement eachResult:AllSearchResults)
			   {
				System.out.println("eachResult.getText:" + eachResult.getText());  
				
				  if (eachResult.getText().equalsIgnoreCase(text))
					{
					    count++;
					    System.out.println("Search found: " + eachResult.getText());
						System.out.println("Produto id: " + count);
						count_p  = count;
						
					}
				  //System.out.println("str01" + eachResult.getText()+ " str02:"+text);
			   }
		}catch(Exception e){
			System.out.println("erro in validate post" +e);
		}
		return count_p;
	}
	
	public String verifiedPassword(String senha) {

		if (StringUtils.isNullOrBlank(senha)){
			System.out.println("senha em branco: "+ senha);
			JPasswordField pf = new JPasswordField();
			int okCxl = JOptionPane.showConfirmDialog(null, pf, "Enter Password", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);

			if (okCxl == JOptionPane.OK_OPTION) {
			  senha = new String(pf.getPassword());
			}
	}
		return senha;
	}
	
	public void movetoelementAndClick(By locator){
		
		System.out.println(locator);
		//to do add wait dynamic.
		Actions actions = new Actions(driver);
		WebElement element = driver.findElement(locator);
		actions.moveToElement(element);
		actions.click();
		actions.perform();
	}
	
	public void movetoelement(By locator){
		
		//to do add wait dynamic.
		System.out.println(locator);
		try{
			Actions actions = new Actions(driver);
			WebElement element = driver.findElement(locator);
			actions.moveToElement(element);
			actions.perform();
			
		}catch(Exception e){
			
			System.out.println("Element not Found! "+e);
		}
	}
	
}
