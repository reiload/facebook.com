package facebook.com.facebook.test;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pageObject.java.com.pageObject.DefaultMenu;
import pageObject.java.com.pageObject.LandingPage;
import pageObject.java.com.pageObject.LoginPage;
import pageObject.java.com.pageObject.PageHeader;
import pageObject.java.com.pageObject.timelinePage;
import pageObject.java.util.Browsers;
import java.io.IOException;


public class StepsDefinition {
	
	LandingPage landingPage;
	LoginPage loginPage;
	DefaultMenu defaultMenu;
	timelinePage timeline_Page;
	PageHeader Page_Header;
	WebDriver driver;
	
	@Given("^Dado o \"([^\"]*)\" e navegue no site www\\.facebook\\.com\\.br$")
	public void setBrowser(String browser) throws Throwable {
		
		// Types of browsers Chrome, Firefox and IE, set in feature.
		driver = (WebDriver) Browsers.setBrowser(browser);

	}

	@Given("^Fazer o \"([^\"]*)\" e \"([^\"]*)\" e valide o \"([^\"]*)\"$")
	public void setLogin(String email, String password, String user_name) throws Throwable {

		// Doing Login
		loginPage = new LoginPage(driver);
		loginPage.login(email).
				password(password).
				loginbutton().
				validate_login(user_name);
	}

	@Given("^Selecione o nome  \"([^\"]*)\" no facebook$")
	public void validationUserName(String user_name) throws Throwable {
		
		defaultMenu = new DefaultMenu(driver);
		defaultMenu.selectText(user_name).validaText(user_name);
		
	}

	@Given("^Validar o Timeline \"([^\"]*)\"$")
	public void validationTimeline(String user_name) throws Throwable {
		
		defaultMenu = new DefaultMenu(driver);
		defaultMenu.validaText(user_name);
	}
	
	@When("^Escrever um novo POST com conteudo \"([^\"]*)\"$")
	public void addNewPosting(String text) throws Throwable {
		
		timeline_Page = new timelinePage(driver);
		timeline_Page.escreverText(text);
		
	}
	
	@Then("^Enviar o novo POST e validar o conteudo \"([^\"]*)\"$")
	public void sendNewPosting(String text) throws Throwable {
		
		timeline_Page = new timelinePage(driver);
		timeline_Page.clickButtonPost().validatePost(text);
		
	}
	
	@Then("^Deletar a nova postagem no facebook e validar a exclusao \"([^\"]*)\"\\.$")
	public void deletePost(String text) throws Throwable {
		
		timeline_Page = new timelinePage(driver);
		timeline_Page.deletePost().
		validateDelete(text);

	}
	
	@Then("^Fazer o logout no facebook e validar o texto \"([^\"]*)\"\\.$")
	public void setLogOut(String text) throws Throwable {
		
		Page_Header = new PageHeader(driver);
		Page_Header.logout().validate_logOut(text);
	}
	
	@Then("^Clique em Like \"([^\"]*)\" no seu POST e valida o \"([^\"]*)\"$")
	public void likePost(String likeText,String likeResult) throws Throwable {
		timeline_Page = new timelinePage(driver);
		timeline_Page.LikePOST(likeText).valideLikePOST(likeResult);
	}
	
	@Then("^Adicione um smile do grupo \"([^\"]*)\" e icone \"([^\"]*)\"$")
	public void addSmile(String groupSmile, String iconSmileFeel) throws Throwable {
		timeline_Page = new timelinePage(driver);
		timeline_Page.addSmile(groupSmile,iconSmileFeel);
	}
	@Then("^Adicione a localizacao \"([^\"]*)\"$")
	public void addLocation(String textLocation) throws Throwable {
		timeline_Page = new timelinePage(driver);
		timeline_Page.addLocation(textLocation);
		
	}
	@Then("^Valide a localizacao da Postagem \"([^\"]*)\"$")
	public void validateLocation(String textLocation) throws Throwable {
		timeline_Page = new timelinePage(driver);
		timeline_Page.validateLocation(textLocation);
	}
    @After
    public void tearDown(Scenario scenario) throws IOException {
	  
		try{
		    // if the scenario fails, it will take a picture.
	        if (scenario.isFailed()) {
	                final byte[] screenshot = ((TakesScreenshot) driver)
	                        .getScreenshotAs(OutputType.BYTES);
	                scenario.embed(screenshot, "image/png");
		            }
	 
		}finally {
		
			/// close the browser.
			//landingPage = new LandingPage(driver);
			//landingPage.closeDriver();
	  	}
  
  }
}